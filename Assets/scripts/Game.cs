using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private int startLivesCount = 3;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinsText;
    private int LivesCount;
    private int coinsCount;

    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        LivesCount = startLivesCount;
        coinsCount = 0;
        ShowLives();
        ShowCoins();
    }

    public void LoseLive()
    {
        LivesCount--;
        ShowLives();
    }

    public void AddCoin(int amount)
    {
        coinsCount += amount;
        ShowCoins();
    }

    private void ShowLives()
    {
        livesText.text = LivesCount.ToString();

    }

    private void ShowCoins()
    {
        coinsText.text = coinsCount.ToString();
    }

} 
